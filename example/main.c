#include "cvector/cvector.h"

#include <stdio.h>

int main()
{
    cvector vector;
    cvec_init(&vector);

    cvec_push_back(&vector, "Apple");
    cvec_push_back(&vector, "Pear");
    cvec_push_back(&vector, "Cherry");
    cvec_push_back(&vector, "Banana");
    cvec_push_back(&vector, "Coconut");

    printf("First round:\n");
    for (unsigned i = 0; i < cvec_size(&vector); ++i) {
        printf("%s\n", (char*)cvec_get(&vector, i));
    }

    cvec_delete(&vector, 3);

    printf("Second round:\n");
    for (unsigned i = 0; i < cvec_size(&vector); ++i) {
        printf("%s\n", (char*)cvec_get(&vector, i));
    }

    cvec_delete(&vector, 3);

    printf("Third round:\n");
    for (unsigned i = 0; i < cvec_size(&vector); ++i) {
        printf("%s\n", (char*)cvec_get(&vector, i));
    }

    cvec_delete(&vector, 1);
    cvec_delete(&vector, 0);

    printf("Fourth round:\n");
    for (unsigned i = 0; i < cvec_size(&vector); ++i) {
        printf("%s\n", (char*)cvec_get(&vector, i));
    }

    cvec_free(&vector);
}
