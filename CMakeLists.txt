cmake_minimum_required(VERSION 3.9)
project(cvector)

set(CMAKE_C_STANDARD 11)
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -Wextra -pedantic -Wshadow")

add_subdirectory(src lib)

option(BUILD_EXAMPLE "Whether to build the example" OFF)
if(${BUILD_EXAMPLE})
    add_subdirectory(example)
endif()
