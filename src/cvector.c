/*
 * Simple vector implementation, based on the code by Emil Hernvall.
 * See https://gist.github.com/EmilHernvall/953968 for original implementation.
 */

#include "cvector.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void cvec_init(cvector* vec)
{
    vec->data = NULL;
    vec->capacity = 0;
    vec->size = 0;
}

unsigned cvec_size(cvector* vec)
{
    return vec->size;
}

unsigned cvec_capacity(cvector* vec)
{
    return vec->capacity;
}

void cvec_push_back(cvector* vec, void* elem)
{
    if (vec->capacity == 0) {
        vec->capacity = 10;
        vec->data = malloc(sizeof (void*) * vec->capacity);
        memset(vec->data, '\0', sizeof (void*) * vec->capacity);
    }

    if (vec->capacity == vec->size) {
        vec->capacity *= 2;
        vec->data = realloc(vec->data, sizeof (void*) * vec->capacity);
    }

    vec->data[vec->size] = elem;
    ++(vec->size);
}

void cvec_set(cvector* vec, unsigned index, void* elem)
{
    if (index >= vec->size) {
        return;
    }

    vec->data[index] = elem;
}

void* cvec_get(cvector* vec, unsigned index)
{
    if (index >= vec->size) {
        return NULL;
    }

    return vec->data[index];
}

void cvec_delete(cvector* vec, unsigned index)
{
    if (index >= vec->size) {
        return;
    }

    for (unsigned i = index, j = index + 1; j < vec->size; ++i, ++j) {
        vec->data[i] = vec->data[j];
    }

    --(vec->size);
}

void cvec_free(cvector* vec)
{
    free(vec->data);
}
