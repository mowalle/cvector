/*
 * Simple vector implementation, based on the code by Emil Hernvall.
 * See https://gist.github.com/EmilHernvall/953968 for original implementation.
 */

#ifndef GUARD_CVECTOR_H
#define GUARD_CVECTOR_H

typedef struct {
    void** data;
    unsigned capacity;
    unsigned size;
} cvector;

void     cvec_init(cvector* vec);
unsigned cvec_size(cvector* vec);
unsigned cvec_capacity(cvector* vec);
void     cvec_push_back(cvector* vec, void* elem);
void     cvec_set(cvector* vec, unsigned index, void* elem);
void*    cvec_get(cvector* vec, unsigned index);
void     cvec_delete(cvector* vec, unsigned index);
void     cvec_free(cvector* vec);

#endif // GUARD_CVECTOR_H
